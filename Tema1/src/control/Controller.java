package control;

import java.awt.event.*; 
import model.*;
import view.*;
public class Controller
{
    private PolinomView theView;
    private Polinom pol1;
    private Polinom pol2;

    private int[] coef1=new int[100];
    private int[] coef2=new int[100];

    private int i, j=0;

    public Controller()
    {
    	//se face legatura dintre butoane si actiuni (logica de program)
        theView=new PolinomView();
        theView.addAddListener(new AddListener());
        theView.addSubListener(new SubListener());
        theView.addSubmit1Listener(new submit1Listener());
        theView.addSubmit2Listener(new submit2Listener());
        theView.addDerListener(new DerListener());
        theView.addResetListener(new ResetListener());
        theView.addMulListener(new MulListener());

    }
    
    //clase interne pentru listeneri
    class submit1Listener implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            if(i<=theView.getGr1())
            {
                coef1[i]=theView.getCoef1();
                theView.updateEticheta1("A fost introdus coef. pt. x^"+i);
                i++;
                System.out.println("A chemat listenerul pt submit1");
            }
            else
                theView.updateEticheta1("Au fost introdusi toti coeficientii");
        }

    }

    class submit2Listener implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            if(j<=theView.getGr2())
            {
                coef2[j]=theView.getCoef2();
                theView.updateEticheta2("A fost introdus coef. pt. x^"+j);
                j++;
                System.out.println("A chemat listenerul pt submit2");
            }
            else
                theView.updateEticheta2("Au fost introdusi toti coeficientii");
        }

    }

    class AddListener implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            pol1=new Polinom(coef1, theView.getGr1());
            pol2=new Polinom(coef2, theView.getGr2());
            pol1.adunare(pol2);
            theView.arataRezultat(pol1);
        }
    }

    class SubListener implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            pol1=new Polinom(coef1, theView.getGr1());
            pol2=new Polinom(coef2, theView.getGr2());
            pol1.scadere(pol2);
            theView.arataRezultat(pol1);
        }
    }
    
    class DerListener implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            pol1=new Polinom(coef1, theView.getGr1());
            pol1.derivare();
            theView.arataRezultat(pol1);
        }
    }
   
    
    class ResetListener implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
        	theView.reset();
        	pol1=null;
        	pol2=null;
        	i=j=0;
        	theView.updateEticheta1("Introduceti coef. pt. x^0");
        	theView.updateEticheta2("Introduceti coef. pt. x^0");
        	theView.updateRezultat("Aici va aparea rezultatul");
        }
    }
    
    class MulListener implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            pol1=new Polinom(coef1, theView.getGr1());
            pol2=new Polinom(coef2, theView.getGr2());
            pol1.inmultire(pol2);
            theView.arataRezultat(pol1);
        }
    }
    
    public static void main(String[] args)
    {
        Controller c=new Controller();

    }
}
