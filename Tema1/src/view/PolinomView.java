package view;

import model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*; 
public class PolinomView extends JFrame
{
    private JLabel gl1=new JLabel("Gradul polinomului 1:");
    private JLabel gl2=new JLabel("Gradul polinomului 2:");
    private JLabel coefCurent1=new JLabel("Introdu coef. pt. x^0");
    private JLabel coefCurent2=new JLabel("Introdu coef. pt. x^0");
    private JLabel cl1=new JLabel("Ultimul coeficient introdus:");
    private JLabel cl2=new JLabel("Ultimul coeficient introdus:");

    private JTextField grad1=new JTextField();
    private JTextField grad2=new JTextField();
    private JTextField coef1=new JTextField();
    private JTextField coef2=new JTextField();

    private JButton submit1=new JButton("Submit1");
    private JButton submit2=new JButton("Submit2");

    private JTextArea rezultat=new JTextArea("Aici va aparea rezultatul");
    private JButton btnPlus=new JButton("+");
    private JButton btnMinus=new JButton("-");
    private JButton btnDerivare=new JButton("Derivare");
    private JButton btnReset=new JButton("Reset");
    private JButton btnMul=new JButton("*");
    


    public PolinomView()
    {
        this.setTitle("Aplicatie pentru procesarea polinoamelor");
        this.setSize(650, 250);
        JPanel content=new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
        JPanel input=new JPanel();
        JPanel output=new JPanel();
        JPanel op=new JPanel();

        input.setLayout(new GridLayout(4, 4));
        input.add(gl1);
        input.add(cl1);
        input.add(coefCurent1);

        input.add(grad1);
        input.add(coef1);
        input.add(submit1);

        input.add(gl2);
        input.add(cl2);
        input.add(coefCurent2);

        input.add(grad2);
        input.add(coef2);
        input.add(submit2);

        output.add(rezultat);

        op.setLayout(new FlowLayout());
        op.add(btnPlus);
        op.add(btnMinus);
        op.add(btnDerivare);
        op.add(btnMul);
        op.add(btnReset);

        content.add(input);
        content.add(output);
        content.add(op);

        this.setContentPane(content);
        this.setVisible(true);
    }

  //metode pentru "exportarea" valorilor de intrare
    public int getCoef1()
    {
        return Integer.parseInt(coef1.getText());
    }

    public int getCoef2()
    {
        return Integer.parseInt(coef2.getText());
    }

    public int getGr1()
    {
        return Integer.parseInt(grad1.getText());
    }

    public int getGr2()
    {
        return Integer.parseInt(grad2.getText());
    }

    //afisarea rezultatului
    public void arataRezultat(Polinom p)
    {
        rezultat.setText(p.toString());
    }

    public void addAddListener(ActionListener plus) {
        btnPlus.addActionListener(plus);
    }
    
    public void addSubListener(ActionListener minus) {
        btnMinus.addActionListener(minus);
    } 
    
    public void addDerListener(ActionListener der) {
        btnDerivare.addActionListener(der);
    } 
    
    public void addSubmit1Listener(ActionListener s1) {
        submit1.addActionListener(s1);
    }
    
    public void addSubmit2Listener(ActionListener s2) {
        submit2.addActionListener(s2);
    }
    
    public void addResetListener(ActionListener rst) {
        btnReset.addActionListener(rst);
    }
    
    public void addMulListener(ActionListener mul) {
        btnMul.addActionListener(mul);
    }
    
    //metode folosite pentru afisarea de mesaje destinate utilizatorului
    public void updateEticheta1(String s)
    {
        coefCurent1.setText(s);
    }
    
    public void updateEticheta2(String s)
    {
        coefCurent2.setText(s);
    }
    
    public void updateRezultat(String s)
    {
        rezultat.setText(s);
    }
    
    public void reset()
    {
    	grad1.setText("");
    	grad2.setText("");
    	coef1.setText("");
    	coef2.setText("");
    }
        
}
