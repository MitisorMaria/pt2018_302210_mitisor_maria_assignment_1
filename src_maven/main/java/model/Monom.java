package model;

public class Monom
{
    private int grad;
    private int coef;

    public Monom(int g, int c)
    {
        grad=g;
        coef=c;
    }
//getteri
    public int getGrad()
    {
        return grad;
    }

    public int getCoef()
    {
        return coef;
    }
//metode de calcul
    public void adunare(Monom m)
    {
        if(m.grad==grad)
        {
            this.coef+=m.coef;
        }
    }  

    public void scadere(Monom m)
    {
        if(m.grad==grad)
        {
            this.coef-=m.coef;
        }
    }  

    public void inmultire(Monom m)
    {
        grad+=m.grad;
        coef*=m.coef;
    }

    public void derivare()
    {
        coef*=grad;
        grad--;
    }

 
//metoda folosita pentru afisarea polinomului
    public String toString()
    {

        String s="";
        if(grad<0)
            return s;
        if((coef>=0)&&(grad!=0))
            s="+";
        s+=coef+"x^"+grad;
        return s;
    }
}
