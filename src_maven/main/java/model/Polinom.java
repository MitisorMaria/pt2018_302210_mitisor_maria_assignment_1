package model;

import java.util.ArrayList;
public class Polinom
{
    private ArrayList<Monom> listaMonoame;
    private int grad;

    //constructor; foloseste vec_coef pentru a crea monoame
    public Polinom(int[] vec_coef, int grad)
    {
        this.grad=grad;
        listaMonoame=new ArrayList<Monom>();
        for(int i=0; i<=grad; i++)
        {
            listaMonoame.add(new Monom(i, vec_coef[i]));
        }

    }
   
    //setteri folositi in special la crearea de copii ale polinoamelor
    public void setGrad(int gr)
    {
    	this.grad=gr;
    }
    
    public void setListaMonoame(ArrayList<Monom> lista)
    {
    	this.listaMonoame=lista;
    }

    
    //metode de calcul
    public void adunare(Polinom p)
    {
        if(p.grad<grad)
        {
        	//primul caz
            for(int i=p.grad; i>=0; i--)
            {
                listaMonoame.get(i).adunare(p.listaMonoame.get(i));
            }
        }
        else
        {
        	//al doilea caz
            for(int i=0; i<=grad; i++)
                listaMonoame.get(i).adunare(p.listaMonoame.get(i));

            for(int i=grad+1; i<=p.grad; i++)
                listaMonoame.add(p.listaMonoame.get(i));
        }

    }

    public void scadere(Polinom p)
    {
        if(p.grad<grad)
        {
        	//primul caz
            for(int i=p.grad; i>=0; i--)
            {
                listaMonoame.get(i).scadere(p.listaMonoame.get(i));
            }
        }
        else
        {
            //al doilea caz
        	for(int i=0; i<=grad; i++)
                listaMonoame.get(i).scadere(p.listaMonoame.get(i));

            for(int i=grad+1; i<=p.grad; i++)
                listaMonoame.add(new Monom(i, (-1)*p.listaMonoame.get(i).getCoef()));
        }

    }

    public void derivare()
    {
        for (Monom m:listaMonoame)
            m.derivare();
    }
    
    public void inmultire(Polinom p2)
    {
    	int gradRez=grad+p2.grad;
    	int[] coefRez=new int[200]; //folosite pentru polinomul auxiliar
    	
    	for(int i=0; i<=gradRez; i++)
    	{
    		coefRez[i]=0;
    	}
    	
    	Polinom Rez=new Polinom(coefRez, gradRez); //se instantiaza polinomul auxiliar
    	for(Monom m1: listaMonoame)
    		for(Monom m2: p2.listaMonoame)
    		{
    			//se fac copii ale monoamelor pentru a nu altera polinomul apelant
    			Monom monomRez1=new Monom(m1.getGrad(), m1.getCoef());
    			Monom monomRez2=new Monom(m2.getGrad(), m2.getCoef());
    			monomRez1.inmultire(monomRez2);
    			int gr=monomRez1.getGrad();
    			Rez.listaMonoame.get(gr).adunare(monomRez1);
    		}
    	this.setGrad(gradRez);
    	this.setListaMonoame(Rez.listaMonoame);
    }

    public String toString()
    {
        String s="";
        for(Monom m: listaMonoame)
            s+=m.toString();
        return s;
    }

}
