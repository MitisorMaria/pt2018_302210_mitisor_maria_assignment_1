package test;

import static org.junit.Assert.*;
import org.junit.*; 
import model.*;

public class JUnitTest {

	private static int nrTesteExecutate = 0;
	private static int nrTesteCuSucces = 0; 
	private static Polinom p1;
	private static Polinom p2;
	
	public JUnitTest() {
		 System.out.println("Constructor inaintea fiecarui test!");
		 }
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	 }
	
	@Before
	public void setUpBeforeTest() throws Exception {
	
	int[] c1=new int[] {1, 1, 1};
	int[] c2=new int[] {1, 1, 1};
	 p1=new Polinom(c1, 2);
	 p2=new Polinom(c2, 2);
	 System.out.println("setUp "+p1.toString());
	 System.out.println("setUp "+p2.toString());
	 }
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	
	 System.out.println("S-au executat " + nrTesteExecutate +  " teste din care "+ nrTesteCuSucces + " au avut succes!");
	 }
	
	@Before
	public void setUp() throws Exception {
	 System.out.println("Incepe un nou test!");
	 nrTesteExecutate++;
	 }
	
	@After
	public void tearDown() throws Exception {
	 System.out.println("S-a terminat testul curent!");
	 }
	
	@Test
	public void testAddGetValue() {
		p1.adunare(p2);
		assertTrue(p1.toString().equals("2x^0+2x^1+2x^2"));
		nrTesteCuSucces++;
	}	
	
	@Test
	public void testSubGetValue() {
		p1.scadere(p2);
		System.out.println(p1.toString());
		assertTrue(p1.toString().equals("0x^0+0x^1+0x^2"));
		nrTesteCuSucces++;
	}	
	
	@Test
	public void testDerGetValue() {
		p1.derivare();
		assertTrue(p1.toString().equals("1x^0+2x^1"));
		nrTesteCuSucces++;
	}	
	
	@Test
	public void testMulGetValue() {
		p1.inmultire(p2);
		assertTrue(p1.toString().equals("1x^0+2x^1+3x^2+2x^3+1x^4"));
		nrTesteCuSucces++;
	}
		
	
}
